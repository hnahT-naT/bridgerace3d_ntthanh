using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Brick : MonoBehaviour
{
    [SerializeField] MeshRenderer meshRenderer;
    internal ColorType color;

    internal void ChangeColor(ColorType color)
    {
        this.color = color;
        int value = (int)color;
        meshRenderer.material = DataManager.Instance.materials[value];
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player"))
        {
            ColorType playerColor = other.gameObject.GetComponent<Player>().color;
            if(playerColor == color)
            {
                other.gameObject.GetComponent<Player>().AddBrick();
                DataManager.Instance.AddBrick(gameObject.transform);
                gameObject.SetActive(false);   
            }
            
        }
        if(other.CompareTag("Enemy"))
        {
            ColorType enemyColor = other.gameObject.GetComponent<Enemy>().color;
            if(enemyColor == color)
            {
                other.gameObject.GetComponent<Enemy>().AddBrick();
                DataManager.Instance.AddBrick(gameObject.transform);
                gameObject.SetActive(false);   
            }
            
        }
    }
}

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour
{
    public int currentStage;
    internal virtual void Victory()
    {
        transform.rotation = Quaternion.LookRotation(new Vector3(0, 0, -1), Vector3.up);
    }

    internal virtual void Lose()
    {
        transform.rotation = Quaternion.LookRotation(new Vector3(0, 0, -1), Vector3.up);
    }
}

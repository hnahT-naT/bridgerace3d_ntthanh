using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player"))
        {
            transform.parent.gameObject.GetComponent<Level>().LoadStage( ++other.GetComponent<Player>().currentStage, other.GetComponent<Player>().color);
        }
        if(other.CompareTag("Enemy"))
        {
            transform.parent.gameObject.GetComponent<Level>().LoadStage(++other.GetComponent<Enemy>().currentStage, other.GetComponent<Enemy>().color);
            other.GetComponent<Enemy>().ChangeState(new FindBrickState());
        }
    }
}

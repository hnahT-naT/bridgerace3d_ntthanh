using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [SerializeField] private GameObject enemyBrickPrefabs; 
    [SerializeField] private float speed = 5f;
    [SerializeField] private LayerMask groundLayer;
    public ColorType color;
    public int currentStage;
    private IState currentState;
    internal Rigidbody rb;
   
    internal bool isMoving;
    internal int brickCount;



    // Start is called before the first frame update
    void Start()
    {
        OnInit();
    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position + Vector3.up * 10, Vector3.down, out hit, Mathf.Infinity, groundLayer))
        {
            transform.position = hit.point + Vector3.up;
        }
        if (currentState != null)
        {
            currentState.OnExecute(this);      // update state hien tai lien tuc
        }
    }

    private void OnInit()
    {
        currentStage = 1;
        brickCount = 0;
        isMoving = false;
        rb = GetComponent<Rigidbody>();
        transform.position = new Vector3(7,1.5f,7);
        ChangeState(new FindBrickState());
    }

    // ham chuyen state
    public void ChangeState(IState newState)
    {   // check state cu 
        if (currentState != null)
        {
            // thoat state cu
            currentState.OnExit(this);
        }
        currentState = newState;
        if (currentState != null)
        {
            // truy cap state moi
            currentState.OnEnter(this);
        }
    }

    internal void MovingToStair(Vector3 target)
    {
        Vector3 direct = Vector3.Normalize(new Vector3(target.x - transform.position.x, 0, target.z - transform.position.z));
        if (Vector2.Distance(new Vector2(transform.position.x, transform.position.z), new Vector2(target.x, target.z)) > 0.1f)
        {
            rb.velocity = direct * Time.deltaTime * speed;
            transform.rotation = Quaternion.LookRotation(direct, Vector3.up);
        }
        if (Vector2.Distance(new Vector2(transform.position.x, transform.position.z), new Vector2(target.x, target.z)) < 0.1f)
        {
            transform.position = new Vector3(target.x, transform.position.y, target.z);
            rb.velocity = Vector3.zero;
            ChangeState(new BuildBridgeState());
        }
    }

    public void MovingToBrick(Vector3 target)
    {
        if(Vector2.Distance(new Vector2(transform.position.x, transform.position.z), new Vector2(target.x, target.z)) < 0.1f)
        {
            isMoving = false;
            rb.velocity = Vector3.zero;
        }
        else
        {
            Vector3 direct =Vector3.Normalize(new Vector3(target.x - transform.position.x, 0, target.z - transform.position.z));
            rb.velocity = direct*Time.deltaTime*speed;
            transform.rotation = Quaternion.LookRotation(direct, Vector3.up);
        }
        
        //transform.position = Vector3.MoveTowards(transform.position, target, speed * Time.deltaTime);
        
    }

    internal void MovingForBuildingStair(Vector3 direct)
    {
        rb.velocity = direct * Time.deltaTime * speed;
        transform.rotation = Quaternion.LookRotation(direct, Vector3.up);
    }    

    internal void AddBrick()
    {
        brickCount++;
        GameObject enemyBrick = Instantiate(enemyBrickPrefabs, transform.position, Quaternion.identity, transform.GetChild(1));
        enemyBrick.transform.localPosition = Vector3.zero + Vector3.up * brickCount * 0.3f;
        enemyBrick.transform.localRotation = Quaternion.Euler(0, 0, 0);
        enemyBrick.transform.GetChild(0).gameObject.GetComponent<MeshRenderer>().material = DataManager.Instance.materials[(int)color];
    }

    internal void RemoveBrick()
    {
        if (brickCount != 0)
        {
            Destroy(transform.GetChild(1).GetChild(brickCount - 1).gameObject);
            brickCount--;
            DataManager.Instance.ReSpawnBrick();
        }
    }
}

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level : MonoBehaviour
{
    public List<Enemy> enemies = new List<Enemy>();
    public List<Stage> stages = new List<Stage>();

    [SerializeField] Player player;
    [SerializeField] List<Transform> startPoint = new List<Transform>();
    // Start is called before the first frame update
    void Start()
    {
        OnInit();
    }

    private void OnInit()
    {
        player = FindObjectOfType<Player>();
        // khoi tao cac khoi stage 1
        for(int i = 0; i <= enemies.Count; i++)
        {
            stages[0].SetColor((ColorType)i);
        }
        // khoi tao mau cho cac enemy
        for(int i = 0; i < enemies.Count; i++)
        {
            enemies[i].color = (ColorType)(i + 1);
        }
        // khoi tao vi tri bat dau cho player va bot
        player.transform.position = startPoint[0].position;
        for(int i = 0; i < enemies.Count; i++)
        {
            enemies[i].transform.position = startPoint[i + 1].position;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    internal void LoadStage(int stage, ColorType color)
    {
        stages[stage-1].SetColor(color);
        stages[stage-2].UnSetColor(color);
    }
}

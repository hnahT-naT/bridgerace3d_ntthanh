using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField] private float speed = 5f;
    [SerializeField] private LayerMask groundLayer;
    [SerializeField] private LayerMask stairBrickLayer;
    [SerializeField] private LayerMask doorLayer;
    [SerializeField] private FixedJoystick joystick;
    [SerializeField] private GameObject playerBrickPrefabs;
    public CharacterState playerState;
    public int currentStage;
    public ColorType color;
    private int brickCount;
    private Rigidbody rb;
    
    // Start is called before the first frame update
    void Start()
    {
        OnInit();
    }
    // Update is called once per frame
    void FixedUpdate()
    {
        if(GameManager.Instance.IsState(GameState.Finish))
        {
            rb.velocity = Vector3.zero;
            return;
        }
        RaycastHit hit;
        if(Physics.Raycast(transform.position + Vector3.up *10, Vector3.down, out hit, Mathf.Infinity, groundLayer))
            {
                transform.position = hit.point + Vector3.up;
            }
        Moving();
    }

     internal void Victory()
    {
        ClearBrick();
        transform.rotation = Quaternion.LookRotation(new Vector3(0, 0, -1), Vector3.up);
    }

    private void ClearBrick()
    {
        while(brickCount != 0)
        {
            Destroy(transform.GetChild(1).GetChild(brickCount - 1).gameObject);
            brickCount--;
        }
    }

    internal void AddBrick()
    {
        brickCount++;
        playerState = CharacterState.UnEmpty;
        GameObject playerBrick = Instantiate(playerBrickPrefabs, transform.position, Quaternion.identity, transform.GetChild(1));
        playerBrick.transform.localPosition = Vector3.zero + Vector3.up * brickCount * 0.3f;
        playerBrick.transform.localRotation = Quaternion.Euler(0, 0, 0);
        playerBrick.transform.GetChild(0).gameObject.GetComponent<MeshRenderer>().material = DataManager.Instance.materials[(int)color];
    }

    internal void RemoveBrick()
    {
        if(brickCount != 0)
        {
        Destroy(transform.GetChild(1).GetChild(brickCount-1).gameObject);
        brickCount--;
            DataManager.Instance.ReSpawnBrick();
        }
        if(brickCount == 0)
        {

            playerState = CharacterState.Empty;
            Debug.Log(playerState);
        }
    }

    private void OnInit()
    {
        playerState = CharacterState.Empty;
        brickCount = 0;
        rb = GetComponent<Rigidbody>();
        currentStage = 1;
    }

    private void Moving()
    {
        float xMove = joystick.Horizontal;
        float zMove = joystick.Vertical;
        // neu bac thang phia trc khong di duoc va zmove > 0
        if(!CheckStairBrick() && zMove > 0)
        {
            zMove = 0;
        }
        // kiem tra co cua phia sau 
        if(CheckDoor() && zMove < 0)
        {
            zMove = 0;
        }
        // add velocity cho player
        rb.velocity = new Vector3(xMove, 0, zMove) * speed * Time.fixedDeltaTime;
        // quay player theo huong di chuyen 
        if (Vector2.Distance(new Vector2(xMove, zMove), Vector2.zero) > 0.1f)
        {
            transform.rotation = Quaternion.LookRotation(new Vector3(xMove, 0, zMove), Vector3.up);
        }
    }
    // kiem tra bac thang phia trc co di duoc khong
    private bool CheckStairBrick()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position - Vector3.up * 1f, Vector3.forward, out hit, 1.1f, stairBrickLayer))
        {
            if (hit.collider != null)
            {
                if (hit.collider.gameObject.GetComponent<StairBrick>().color != color && playerState == CharacterState.Empty)
                {
                    return false;
                }
            }
        }
        return true;
    }
    // kiem tra co cua phia sau hay khong
    private bool CheckDoor()
    {
        return Physics.Raycast(transform.position, Vector3.back, 1.01f, doorLayer);
    }
}

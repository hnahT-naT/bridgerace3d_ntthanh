using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage : MonoBehaviour
{
    [SerializeField] private Transform upperRightPoint;
    [SerializeField] private Transform lowerLeftPoint;
    [SerializeField] private float hozirontalDistance;
    [SerializeField] private float verticalDistance;
    [SerializeField] private GameObject brick;
    public List<Transform> stairPos = new List<Transform>();
    internal List<Transform> brickList = new List<Transform>();

    internal int colorCount;

    // Start is called before the first frame update
    void Awake()
    {
        OnInit();
    }

    void Update()
    {
        
    }

    private void OnInit()
    {
        colorCount = GetComponentInParent<Level>().enemies.Count + 1;
        SpawnTotalBrick();
    }

    private void SpawnTotalBrick()
    {
        for(int i = 0; i<200;  i++)
        {
            if((i*hozirontalDistance) > Mathf.Abs(upperRightPoint.position.x - lowerLeftPoint.position.x))
            {
                break;
            }
            for(int j = 0; j<200; j++)
            {
                Vector2 generatePoint = new Vector2(i * hozirontalDistance, j * verticalDistance);
                if( generatePoint.y > Mathf.Abs(upperRightPoint.position.z - lowerLeftPoint.position.z))
                {
                    break; 
                }
                GameObject newBrick = Instantiate(brick, new Vector3(generatePoint.x, 0, generatePoint.y) + lowerLeftPoint.position, Quaternion.identity);
                newBrick.SetActive(false);
                brickList.Add(newBrick.transform);
            } 
        }

        // random brickList
        for (int i = 0; i < brickList.Count-1; i++)
        {
            int k = Random.Range(i+1,brickList.Count);
            Transform value = brickList[i];
            brickList[i] = brickList[k];
            brickList[k] = value;
        }
    }

    internal void SetColor(ColorType color)
    {
        int colorNums = brickList.Count / colorCount;
        for(int i = colorNums* (int)color; i < colorNums*((int)color+1); i++)
        {
            brickList[i].gameObject.GetComponent<Brick>().ChangeColor(color);
            brickList[i].gameObject.SetActive(true);
        }
    }

    internal void UnSetColor(ColorType color)
    {
        int colorNums = brickList.Count / colorCount;
        for (int i = colorNums * (int)color; i < colorNums * ((int)color + 1); i++)
        {
            brickList[i].gameObject.SetActive(false);
        }
    }

}

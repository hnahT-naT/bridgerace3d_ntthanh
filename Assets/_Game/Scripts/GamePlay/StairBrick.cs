using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StairBrick : MonoBehaviour
{
    [SerializeField] MeshRenderer meshRenderer;
    internal ColorType color;
    [SerializeField] private LayerMask stairBrickLayer;
    private void Start()
    {
        meshRenderer.gameObject.SetActive(false);
        color = ColorType.Default;
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            RaycastHit hit;
            if(Physics.Raycast(other.transform.position, Vector3.down, out hit, Mathf.Infinity, stairBrickLayer))
            {
                if (hit.transform.gameObject == transform.gameObject)
                {
                    return;
                }
            }
            ColorType playerColor = other.gameObject.GetComponent<Player>().color;
            if (color != playerColor && other.gameObject.GetComponent<Player>().playerState != CharacterState.Empty)
            {
                other.gameObject.GetComponent<Player>().RemoveBrick();
                meshRenderer.gameObject.SetActive(true);
                meshRenderer.material = DataManager.Instance.materials[(int)playerColor];
                color = playerColor;
            }
        }
        if (other.CompareTag("Enemy"))
        {
            RaycastHit hit;
            if(Physics.Raycast(other.transform.position, Vector3.down, out hit, Mathf.Infinity, stairBrickLayer))
            {
                if (hit.transform.gameObject == transform.gameObject)
                {
                    return;
                }
            }
            ColorType enemyColor = other.gameObject.GetComponent<Enemy>().color;
            if (color != enemyColor)
            {
                other.gameObject.GetComponent<Enemy>().RemoveBrick();
                meshRenderer.gameObject.SetActive(true);
                meshRenderer.material = DataManager.Instance.materials[(int)enemyColor];
                color = enemyColor;
            }
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildBridgeState : IState
{
    internal Vector3 startPoint;
    internal Vector3 direct;
    public void OnEnter(Enemy enemy)
    {
        startPoint = enemy.transform.position;
        direct = Vector3.forward;
    }

    public void OnExecute(Enemy enemy)
    {
        if(enemy.brickCount == 0)
        {
            direct = Vector3.back;
        }
        enemy.MovingForBuildingStair(direct);
        if(Vector3.Distance(enemy.transform.position, startPoint) < 0.1f && enemy.brickCount == 0)
        {
            enemy.transform.position = startPoint;
            enemy.ChangeState(new FindBrickState());
        }
    }

    public void OnExit(Enemy enemy)
    {

    }
}

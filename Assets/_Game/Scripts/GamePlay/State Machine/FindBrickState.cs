using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FindBrickState : IState
{
    private Stage currentStage;
    private ColorType enemyColor;
    private float timeCount;
    private float timer;
    private Transform target;
    private Vector3 targetStair;
    private Vector3 targetPos;
    public void OnEnter(Enemy enemy)
    {
        currentStage = enemy.transform.parent.gameObject.GetComponent<Level>().stages[enemy.currentStage - 1];
        enemyColor = enemy.color;
        timeCount = 0;
        timer = Random.Range(5f, 10f);
        target = null;
        enemy.isMoving = false;
        targetStair = Vector3.zero;
        targetPos = new Vector3(Mathf.Infinity, Mathf.Infinity, Mathf.Infinity);
    }

    public void OnExecute(Enemy enemy)
    {
        timeCount += Time.deltaTime;
        int colorNums = currentStage.brickList.Count / currentStage.colorCount;
        if(Vector3.Distance(targetStair, Vector3.zero) > 0.1f)
        {
            enemy.MovingToStair(targetStair);
            return;
        }
        if(timeCount > timer)
        {
            Debug.Log(timeCount);
            Debug.Log(enemy.brickCount);
            int i = Random.Range(0, currentStage.stairPos.Count);
            targetStair = currentStage.stairPos[i].position;
            Debug.Log(targetStair);
            return;
        }
        if(enemy.isMoving && target != null && target.gameObject.activeInHierarchy == true)
        {
            enemy.MovingToBrick(target.position);
            return;
        }
        target = null;
        targetPos = new Vector3(Mathf.Infinity, Mathf.Infinity, Mathf.Infinity);
        for (int i = colorNums*(int)enemyColor; i<colorNums*((int)enemyColor+1); i++)
        {
            if(currentStage.brickList[i].gameObject.activeInHierarchy == false)
            {
                continue;
            }
            if(Vector3.Distance(enemy.transform.position, currentStage.brickList[i].position) < Vector3.Distance(enemy.transform.position, targetPos))
            {
                target = currentStage.brickList[i];
                targetPos = target.position;
            }
        }
        if(target == null)
        {
            enemy.rb.velocity = Vector3.zero;
        }
        else if (Vector3.Distance(enemy.transform.position, targetPos) > 0.1f)
        {
            enemy.isMoving = true;
        }
    }

    public void OnExit(Enemy enemy)
    {

    }
}

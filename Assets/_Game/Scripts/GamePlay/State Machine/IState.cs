using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IState // ham interface
{
    void OnEnter(Enemy enemy); // bat dau vao state

    void OnExecute(Enemy enemy); // update state

    void OnExit(Enemy enemy); // thoat khoi state

}

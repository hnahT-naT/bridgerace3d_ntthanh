using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinPos : MonoBehaviour
{
    private Vector3 firstPosition, secondPosition, thirdPosition;
    // Start is called before the first frame update
    void Start()
    {
        OnInit();
    }
    void OnInit()
    {
        firstPosition = transform.GetChild(0).position + Vector3.up * 5.5f;
        secondPosition = transform.GetChild(1).position + Vector3.up * 5.5f;
        thirdPosition = transform.GetChild(2).position + Vector3.up * 5.5f;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            other.transform.position = firstPosition;
            other.GetComponent<Player>().Victory();
            GameManager.Instance.ChangeState(GameState.Finish);
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ColorType { Red, Green, Blue, Default}
public enum CharacterState { Empty, UnEmpty}
public enum LevelStage {Stage_1, Stage_2, Stage_3}
public class DataManager : Singleton<DataManager>
{
    public List<Material> materials = new List<Material>();
    public Stack<Transform> brickPosition = new Stack<Transform>();
    public Player player;
    // Start is called before the first frame update
    void Awake()
    {

    }

    public void AddBrick(Transform brick)
    {
        brickPosition.Push(brick);
    }

    public void ReSpawnBrick()
    {
        Transform T = brickPosition.Pop();
        T.gameObject.SetActive(true);
    }

    public void ChangeState(CharacterState name, CharacterState state)
    {
        name = state;
    }
    public bool IsState(CharacterState name, CharacterState state)
    {
        return name == state;
    }
    
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : Singleton<LevelManager>
{
    [SerializeField] List<Level> levels = new List<Level>();
    // Start is called before the first frame update
    void Awake()
    {
        Instantiate(levels[0]);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

}

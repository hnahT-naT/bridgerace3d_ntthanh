using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : Singleton<UIManager>
{
    List<UICanvas> UIPrefabs = new List<UICanvas>();
    Transform canvasParent;
    private bool[] isLoad;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void OpenUI<T>() where T : UICanvas
    {
        // lay UI minh can 
        UICanvas canvas = GetUI<T>();
        canvas.setup();
        canvas.open();
    }

    public T GetUI<T>() where T : UICanvas
    {
        for(int i = 0; i < UIPrefabs.Count; i++)
        {
            if(UIPrefabs is T)
            {
                if(!isLoad[i])
                {
                    Instantiate(UIPrefabs[i], canvasParent);
                }
            }
        }
    }
}
